/**
 * Expected behaviour: show an info message, then alert, then error
 */

let Messages = require("../../../src/js/view/Messages");

let m1 = new Messages("messages");
m1.show("Some info text", "info", 3000);
m1.show("Some alert text", "alert", 3000);

setTimeout(() => {
		m1.show("Some error text", "error", 5000);
	},
	8000
);
