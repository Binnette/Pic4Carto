/*
 * Expected behaviour: show time filter, and display alert when time range changed
 */

let L = require("leaflet");
require("../../../src/js/view/TimeFilter");

//Map init
let map = L.map("map").setView(L.latLng(48.11, -1.65), 12);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	name: "OpenStreetMap",
	attribution: "Tiles <a href=\"http://openstreetmap.org/\">OSM</a>",
	maxZoom: 19
}).addTo(map);

let tf = L.control.timeFilter();
tf.addTo(map);
tf.on("timefiltered", d => {
	console.log(d);
});

//Change time
setTimeout(() => {
	tf.setDateRange(new Date(Date.now() - 1000*60*60*24*30*6), new Date());
}, 2000);

setTimeout(() => {
	tf.setDateRange(null, new Date());
}, 4000);

setTimeout(() => {
	tf.setDateRange(new Date(Date.now() - 1000*60*60*24*30*6), null);
}, 6000);
