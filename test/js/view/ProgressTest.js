/*
 * Expected behaviour: show the progress bar with value 0, then 42, then 100, then 33 with custom text.
 */

let Progress = require("../../../src/js/view/Progress");

let p = new Progress("progress");

setTimeout(() => { p.set(42); }, 2000);
setTimeout(() => { p.set(100); }, 4000);
setTimeout(() => { p.set(33); p.setText("123/456"); }, 6000);
