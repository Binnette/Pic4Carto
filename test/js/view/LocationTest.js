/*
 * Expected behaviour: show a simple location, then another one with a direction
 */

let L = require("leaflet");
let Cell = require("../../../src/js/model/Cell");
let Picture = require("../../../src/js/model/Picture");
let Location = require("../../../src/js/view/Location");

let cell = new Cell(L.latLngBounds(L.latLng(47.527, -3.146), L.latLng(47.528, -3.145)));
cell.addPicture(new Picture(
	"https://d1cuyjsrcm0gby.cloudfront.net/jV51SzNSdbkyjB3YonwkbQ/thumb-2048.jpg",
	123456789,
	L.latLng(47.527285430000006, -3.1456470699999954),
	"Mapillary",
	"",
	"",
	"",
	270
));
cell.addPicture(new Picture(
	"https://d1cuyjsrcm0gby.cloudfront.net/LXNbx2GO1GWl8BqdDdoN0w/thumb-2048.jpg",
	123456790,
	L.latLng(47.527336409999975, -3.1457141999999294),
	"Mapillary",
	"",
	"",
	"",
	180
));
cell.addPicture(new Picture(
	"https://d1cuyjsrcm0gby.cloudfront.net/eSowKI5VagzLQaHFXq2rqQ/thumb-2048.jpg",
	123456790,
	L.latLng(47.52736066, -3.1457892099999754),
	"Mapillary",
	"",
	"",
	"",
	90
));

let l1 = new Location("location", cell);
l1.setCurrentPicture(1);
l1.on("click", d => { console.log(d.pictureId); });

setTimeout(() => { l1.setCurrentPicture(0); }, 3000);
setTimeout(() => { l1.setCurrentPicture(2); }, 6000);
