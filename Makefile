# Pic4Carto
# Efficient pictures viewer for micro-mapping in OpenStreetMap
# Author: Adrien PAVIE
#
# Makefile

# Folders
SRC_FOLDER=./src
DIST_FOLDER=./dist
TEST_FOLDER=./test
NM_FOLDER=./node_modules
IMG_FOLDER=$(SRC_FOLDER)/img

# Files
TEST_FILES = \
	$(TEST_FOLDER)/js/model/*.js \
	$(TEST_FOLDER)/js/ctrl/service/*.js

# Binaries
BROWSERIFY=$(NM_FOLDER)/browserify/bin/cmd.js
REWORKNPM=$(NM_FOLDER)/rework-npm-cli/index.js
UGLIFYCSS=$(NM_FOLDER)/uglifycss/uglifycss
UGLIFYJS=$(NM_FOLDER)/uglify-js-harmony/bin/uglifyjs
WATCHIFY=$(NM_FOLDER)/watchify/bin/cmd.js

# Options
BABELIFY_TRANSFORM=-t [ babelify --presets [ latest stage-0 ] ]

# Targets
CSS_OUT=$(DIST_FOLDER)/P4C.css
CSS_MIN_OUT=$(DIST_FOLDER)/P4C.min.css
DIST_ZIP=./dist.zip
MAIN_JS_OUT=$(DIST_FOLDER)/P4C.Main.js
PLAYER_JS_OUT=$(DIST_FOLDER)/P4C.Player.js
TEST_JS=$(TEST_FOLDER)/unit_testing.js


# Tasks
all: deps assets js distzip
debug: cssdebug jsdebug
dev: assets npmlink js

deps:
	npm install

assets: distfolder html images data nm_assets css

npmlink:
	npm link pic4carto

distfolder:
	mkdir -p $(DIST_FOLDER)

html:
	cp $(SRC_FOLDER)/*.html $(DIST_FOLDER)/

images:
	cp -r $(IMG_FOLDER) $(DIST_FOLDER)/
	rm -f $(DIST_FOLDER)/img/*.svg

data:
	cp -r ./assets/* $(DIST_FOLDER)/

nm_assets:
	cp $(NM_FOLDER)/leaflet/dist/images/* $(DIST_FOLDER)/img/
	cp $(NM_FOLDER)/leaflet-geosearch/src/img/* $(DIST_FOLDER)/img/

cssdebug:
	$(REWORKNPM) $(SRC_FOLDER)/css/P4C.css > $(CSS_OUT)
	sed -i 's#images/#img/#g;s#../img/#img/#g' $(CSS_OUT)

css:
	$(REWORKNPM) $(SRC_FOLDER)/css/P4C.css | sed 's#images/#img/#g;s#../img/#img/#g' | $(UGLIFYCSS) --ugly-comments > $(CSS_MIN_OUT)

jsdebug:
	$(BROWSERIFY) $(BABELIFY_TRANSFORM) $(SRC_FOLDER)/js/ctrl/Main.js --debug --s Main > $(MAIN_JS_OUT)
	$(BROWSERIFY) $(BABELIFY_TRANSFORM) $(SRC_FOLDER)/js/ctrl/Player.js --debug --s Player > $(PLAYER_JS_OUT)

js:
	$(BROWSERIFY) $(SRC_FOLDER)/js/ctrl/Main.js --s Main $(BABELIFY_TRANSFORM)  | $(UGLIFYJS) -c > $(MAIN_JS_OUT)
	$(BROWSERIFY) $(SRC_FOLDER)/js/ctrl/Player.js --s Player $(BABELIFY_TRANSFORM) | $(UGLIFYJS) -c > $(PLAYER_JS_OUT)

distzip:
	rm -f $(DIST_ZIP)
	zip -9 -r $(DIST_ZIP) $(DIST_FOLDER)/


# Tests
test: unittest

unittest:
	$(BROWSERIFY) --debug $(TEST_FILES) $(BABELIFY_TRANSFORM) > $(TEST_JS)

viewtest:
	$(BROWSERIFY) --debug $(TEST_FOLDER)/js/view/$(name)Test.js $(BABELIFY_TRANSFORM) > $(TEST_FOLDER)/View_$(name)Test.js


# Watch
watchmain:
	$(WATCHIFY) --debug $(SRC_FOLDER)/js/ctrl/Main.js -o $(MAIN_JS_OUT) --s Main -v $(BABELIFY_TRANSFORM)

watchplayer:
	$(WATCHIFY) --debug $(SRC_FOLDER)/js/ctrl/Player.js -o $(PLAYER_JS_OUT) --s Player -v $(BABELIFY_TRANSFORM)

watchunittest:
	$(WATCHIFY) --debug $(TEST_FILES) -o $(TEST_JS) -v $(BABELIFY_TRANSFORM)

watchviewtest:
	$(WATCHIFY) --debug $(TEST_FOLDER)/js/view/$(name)Test.js -o $(TEST_FOLDER)/View_$(name)Test.js -v $(BABELIFY_TRANSFORM)


# Cleaning
clean:
	rm -rf $(DIST_FOLDER)
	rm -f $(DIST_ZIP)
	rm -f $(TEST_FOLDER)/*.js
