/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let L = require("leaflet");
let Cell = require("./Cell");

/**
 * GridFactory creates a set of cells, fully covering a given area.
 */
let GridFactory = {
	/**
	 * Creates a grid (set of cells) covering the given bounding box
	 * @param b The area to cover
	 * @param {Object} [cellOptions] Some parameters to set in generated cells
	 * @return An array of cells
	 */
	make: function(b, cellOptions) {
		let bbox = b.pad(0);
		let precision = 2;
		let f = (v) => { return +v.toFixed(precision+1); }
		let step = f(0.5 * Math.pow(10, -precision));
		cellOptions = cellOptions || {};
		
		//Extend bounding box to make an exact match
		if(f(bbox.getSouth()) / step != Math.floor(f(bbox.getSouth() / step))) {
			bbox._southWest.lat = f(Math.floor(bbox.getSouth() / step) * step);
		}
		if(f(bbox.getWest()) / step != Math.floor(f(bbox.getWest() / step))) {
			bbox._southWest.lng = f(Math.floor(bbox.getWest() / step) * step);
		}
		if(f(bbox.getNorth()) / step != Math.ceil(f(bbox.getNorth() / step))) {
			bbox._northEast.lat = f(Math.ceil(bbox.getNorth() / step) * step);
		}
		if(f(bbox.getEast()) / step != Math.ceil(f(bbox.getEast() / step))) {
			bbox._northEast.lng = f(Math.ceil(bbox.getEast() / step) * step);
		}
		
		let grid = [];
		
		//Create cells geometries
		for(let lng = f(bbox.getWest()); f(lng+step) <= f(bbox.getEast()); lng += step) {
			for(let lat = f(bbox.getSouth()); f(lat+step) <= f(bbox.getNorth()); lat += step) {
				grid.push(new Cell(
					L.latLngBounds(
						L.latLng(
							f(lat),
							f(lng)
						),
						L.latLng(
							f(lat+step),
							f(lng+step)
						)
					),
					cellOptions
				));
			}
		}
		
		return grid;
	}
};

module.exports = GridFactory;
