/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A cell is a square area, containing several recent pictures.
 */
class Cell {
//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param bounds The geometry of this cell
	 * @param {Object} [options] Any kind of key=value options associated to the cell
	 */
	constructor(bounds, options) {
		if(bounds === undefined || bounds === null || !bounds.toBBoxString) {
			throw new Error("model.cell.invalidbounds");
		}
		
		this.creationDate = Date.now();
		this.pictures = [];
		this.bounds = bounds;
		this.options = options || {};
	}

//ACCESSORS
	/**
	 * @return An unique identifier for this cell area
	 */
	getId() {
		let cellId = "Cell#"+this.bounds.toBBoxString();
		for(const p in this.options) {
			cellId += "#"+p+"="+this.options[p];
		}
		return cellId;
	}
	
	/**
	 * @return The pictures, sorted by inverted chronological order
	 */
	getPictures() {
		return this.pictures.sort((a,b) => {
			return b.date - a.date;
		});
	}
	
	/**
	 * @return This object in a JSON representation
	 */
	toJSONString() {
		return JSON.stringify(this);
	}

//MODIFIERS
	/**
	 * Add a picture to the given cell
	 * @param p The picture to add
	 */
	addPicture(p) {
		this.pictures.push(p);
	}
}

module.exports = Cell;
