/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ProviderFilter is a Leaflet control for choosing which picture providers to use.
 */
L.Control.ProviderFilter = L.Control.extend({
	options: {
		position: 'topright'
	},
	
	initialize: function (options) {
		if(!options.providers) {
			throw new Error("Missing providers option");
		}
		
		this._callbacks = {};
		this._checkboxes = {};
		
		L.Util.setOptions(this, options);
		
		for(const p in this.options.providers) {
			this.options.providers[p].selected = true;
		}
	},

	onAdd: function (map) {
		this._map = map;
		this._container = L.DomUtil.create('div', 'leaflet-control-providerfilter leaflet-bar');
		this._initDom();
		return this._container;
	},

	onRemove: function (map) {
	},
	
	/**
	 * Inverts visibility of button and providers selector.
	 */
	toggle: function() {
		if(this._isExpanded()) {
			L.DomUtil.removeClass(this._buttonContainer, 'hidden');
			L.DomUtil.addClass(this._filterContainer, 'hidden');
		}
		else {
			L.DomUtil.addClass(this._buttonContainer, 'hidden');
			L.DomUtil.removeClass(this._filterContainer, 'hidden');
		}
	},
	
	_isExpanded: function() {
		return L.DomUtil.hasClass(this._buttonContainer, 'hidden');
	},
	
	/**
	 * Returns the currently selected providers
	 * @return {string[]} The latest selected providers, as an array of provider ID
	 */
	getProviders: function() {
		const selected = [];
		for(const p in this.options.providers) {
			if(this.options.providers[p].selected) {
				selected.push(p);
			}
		}
		return selected;
	},
	
	/**
	 * Change the currently selected providers.
	 * @param {string[]} providers The list of providers
	 */
	setProviders: function(providers) {
		for(const p in this.options.providers) {
			if(providers.indexOf(p) >= 0) {
				this.options.providers[p].selected = true;
				this._checkboxes[p].checked = true;
			}
			else {
				this.options.providers[p].selected = false;
				this._checkboxes[p].checked = false;
			}
		}
	},
	
	_initDom: function() {
		//Out of container event
		L.DomEvent.addListener(this._container, 'mouseleave', function() {
			if(this._isExpanded()) {
				this.toggle();
				this.fire("providerfiltered", this.getProviders());
			}
		}.bind(this));
		
		/*
		 * Button
		 */
		//Container
		this._buttonContainer = L.DomUtil.create('div', 'leaflet-control-providerfilter-button', this._container);
		
		//Link
		var button = L.DomUtil.create('a', '', this._buttonContainer);
		L.DomEvent.addListener(button, 'mouseenter', function() {
			if(!this._isExpanded()) {
				this.toggle();
			}
		}.bind(this));
		
		//Link image
		var buttonImg = L.DomUtil.create('img', '', button);
		buttonImg.src = "img/providers.32.png";
		buttonImg.alt = "P";
		buttonImg.title = "Picture providers filter";
		
		/*
		 * Provider filter
		 */
		//Container
		this._filterContainer = L.DomUtil.create('div', 'leaflet-control-providerfilter-filter', this._container);
		L.DomUtil.addClass(this._filterContainer, 'hidden');
		
		//Create option for each provider
		for(const p in this.options.providers) {
			const pContainer = L.DomUtil.create('span', 'leaflet-control-providerfilter-provider', this._filterContainer);
			
			//Checkbox
			const pCheck = L.DomUtil.create('input', null, pContainer);
			pCheck.type = "checkbox";
			pCheck.name = "provider";
			pCheck.value = p;
			pCheck.id = "provider-"+p;
			pCheck.checked = this.options.providers[p].selected;
			
			L.DomEvent.addListener(pCheck, 'change', function() {
				this.options.providers[p].selected = pCheck.checked;
			}.bind(this));
			L.DomEvent.disableClickPropagation(pCheck);
			
			this._checkboxes[p] = pCheck;
			
			//Label
			const pLabel = L.DomUtil.create('label', null, pContainer);
			pLabel.htmlFor = pCheck.id;
			L.DomEvent.disableClickPropagation(pLabel);
			
			//Image
			const pImg = L.DomUtil.create('img', null, pLabel);
			pImg.src = this.options.providers[p].logoUrl;
			pImg.alt = this.options.providers[p].name;
			pImg.title = this.options.providers[p].name;
		}
	},
	
	on: function(event, callback) {
		if(this._callbacks[event] === undefined) {
			this._callbacks[event] = [ callback ];
		}
		else {
			this._callbacks[event].push(callback);
		}
	},
	
	off: function(event, callback) {
		if(callback != null) {
			var id = this._callbacks[event].indexOf(callback);
			if(id >= 0) {
				this._callbacks[event].splice(id, 1);
			}
		}
		else {
			this._callbacks[event] = undefined;
		}
	},
	
	fire: function(event, data) {
		if(this._callbacks[event] != undefined) {
			for(var i = 0; i < this._callbacks[event].length; i++) {
				this._callbacks[event][i](data);
			}
		}
	}
});

L.control.providerFilter = function(options) {
	options = options || {};
	return new L.Control.ProviderFilter(options);
};
