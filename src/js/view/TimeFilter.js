/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let Pikaday = require("pikaday");

/**
 * TimeFilter is a Leaflet control for choosing a time range.
 */
L.Control.TimeFilter = L.Control.extend({
	options: {
		position: 'topright'
	},
	
	initialize: function (options) {
		L.Util.setOptions(this, options);
		this._callbacks = {};
		this._prevRange = { from: null, to: null };
		this._currentRange = { from: null, to: null };
	},

	onAdd: function (map) {
		this._map = map;
		this._container = L.DomUtil.create('div', 'leaflet-control-timefilter leaflet-bar');
		this._initDom();
		return this._container;
	},

	onRemove: function (map) {
	},
	
	/**
	 * Inverts visibility of button and time pickers.
	 */
	toggle: function() {
		if(L.DomUtil.hasClass(this._filterContainer, 'hidden')) {
			L.DomUtil.removeClass(this._filterContainer, 'hidden');
		}
		else {
			L.DomUtil.addClass(this._filterContainer, 'hidden');
		}
	},
	
	/**
	 * Returns the latest validated time range.
	 * @return {Object} The latest time range { from: Date, to: Date }
	 */
	getDateRange: function() {
		return this._prevRange;
	},
	
	/**
	 * Change the currently shown date range.
	 * @param {Date} [start] The start date
	 * @param {Date} [end] The end date
	 */
	setDateRange: function(start, end) {
		let myStart = (start) ? new Date(start.getTime() + start.getTimezoneOffset() * 60 * 1000) : null;
		let myEnd = (end) ? new Date(end.getTime() + end.getTimezoneOffset() * 60 * 1000) : null;
		
		this._fromPicker.setDate(myStart);
		this._toPicker.setDate(myEnd);
		
		this._fromPicker.setMaxDate(myEnd);
		this._fromPicker.setEndRange(myEnd);
		if(!myStart) {
			this._fromPicker.setMinDate(myStart);
			this._fromPicker.setStartRange(myStart);
		}
		
		this._toPicker.setMinDate(myStart);
		this._toPicker.setStartRange(myStart);
		if(!myEnd) {
			this._toPicker.setMaxDate(myEnd);
			this._toPicker.setEndRange(myEnd);
		}
		
		this._prevRange = { from: start, to: end };
		this._currentRange = { from: start, to: end };
	},
	
	_initDom: function() {
		/*
		 * Button
		 */
		//Container
		this._buttonContainer = L.DomUtil.create('div', 'leaflet-control-timefilter-button', this._container);
		
		//Link
		var button = L.DomUtil.create('a', '', this._buttonContainer);
		L.DomEvent.addListener(button, 'click', function() {
			if(!L.DomUtil.hasClass(this._filterContainer, 'hidden')) {
				this.setDateRange(this._prevRange.from, this._prevRange.to);
			}
			this.toggle();
		}.bind(this));
		L.DomEvent.disableClickPropagation(button);
		
		//Link image
		var buttonImg = L.DomUtil.create('img', '', button);
		buttonImg.src = "img/date_range.32.png";
		buttonImg.alt = "T";
		buttonImg.title = "Time filter";
		
		/*
		 * Time filter
		 */
		//Container
		this._filterContainer = L.DomUtil.create('div', 'leaflet-control-timefilter-filter', this._container);
		L.DomUtil.addClass(this._filterContainer, 'hidden');
		
		//From input
		var fromInput = L.DomUtil.create('input', 'leaflet-control-timefilter-from', this._filterContainer);
		fromInput.type = "text";
		fromInput.placeholder = "From...";
		L.DomEvent.disableClickPropagation(fromInput);
		
		var updateFromDate = function(d) {
			fromPicker.setStartRange(d);
			toPicker.setStartRange(d);
			toPicker.setMinDate(d);
			this._currentRange.from = d ? new Date(d.getTime() - d.getTimezoneOffset() * 60 * 1000) : null;
		}.bind(this);
		
		var fromPicker = new Pikaday({ field: fromInput, firstDay: 1, onSelect: function() {
			updateFromDate(this.getDate());
		} });
		
		L.DomEvent.addListener(fromInput, 'input', function() {
			var dOrig = fromInput.value.trim();
			var d = (dOrig.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) ? new Date(dOrig) : null;
			updateFromDate(d);
			fromPicker.setDate(d);
			fromInput.value = dOrig;
		});
		
		this._fromPicker = fromPicker;
		
		//To input
		var toInput = L.DomUtil.create('input', 'leaflet-control-timefilter-to', this._filterContainer);
		toInput.type = "text";
		toInput.placeholder = "To...";
		L.DomEvent.disableClickPropagation(toInput);
		
		var updateToDate = function(d) {
			fromPicker.setEndRange(d);
			fromPicker.setMaxDate(d);
			toPicker.setEndRange(d);
			this._currentRange.to = d ? new Date(d.getTime() - d.getTimezoneOffset() * 60 * 1000) : null;
		}.bind(this);
		
		var toPicker = new Pikaday({ field: toInput, firstDay: 1, onSelect: function() {
			updateToDate(this.getDate());
		} });
		
		L.DomEvent.addListener(toInput, 'input', function() {
			var dOrig = toInput.value.trim();
			var d = (dOrig.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) ? new Date(dOrig) : null;
			updateToDate(d);
			toPicker.setDate(d);
			toInput.value = dOrig;
		});
		
		this._toPicker = toPicker;
		
		
		//Valid button
		var valid = L.DomUtil.create('button', 'leaflet-control-timefilter-valid', this._filterContainer);
		valid.innerHTML = "OK";
		L.DomEvent.addListener(valid, 'click', this._validDate.bind(this));
	},
	
	_validDate: function() {
		this.toggle();
		this._prevRange = this._currentRange;
		this.fire("timefiltered", this._currentRange);
	},
	
	on: function(event, callback) {
		if(this._callbacks[event] === undefined) {
			this._callbacks[event] = [ callback ];
		}
		else {
			this._callbacks[event].push(callback);
		}
	},
	
	off: function(event, callback) {
		if(callback != null) {
			var id = this._callbacks[event].indexOf(callback);
			if(id >= 0) {
				this._callbacks[event].splice(id, 1);
			}
		}
		else {
			this._callbacks[event] = undefined;
		}
	},
	
	fire: function(event, data) {
		if(this._callbacks[event] != undefined) {
			for(var i = 0; i < this._callbacks[event].length; i++) {
				this._callbacks[event][i](data);
			}
		}
	}
});

L.control.timeFilter = function(options) {
	options = options || {};
	return new L.Control.TimeFilter(options);
};
