/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let FileSaver = require("file-saver");
let L = require("leaflet");
let osm_geojson = require("osm-and-geojson");

/**
 * Create a GeoJSON representation of the grid
 * @param cells The array of cells
 * @return A GeoJSON object
 */
let makeGeoJSON = function(cells) {
	let geojson = {
		features: [],
		type: "FeatureCollection"
	};
	
	//Add each cell to GeoJSON object
	for(let i=0; i < cells.length; i++) {
		let cell = cells[i];
		geojson.features.push(
			L.rectangle(cell.bounds).toGeoJSON()
		);
	}
	
	return geojson;
};

/**
 * Makes a grid of cells downloadable for users.
 * It offers different export formats.
 */
let GridExporter = {
	/**
	 * Export as GeoJSON
	 * @param cells An array of cells
	 */
	exportAsGeoJSON: function(cells) {
		//Export
		let blob = new Blob(
						[JSON.stringify(makeGeoJSON(cells))],
						{type: "application/json;charset=utf-8"}
					);
		FileSaver.saveAs(blob, "grid.geojson");
	},
	
	/**
	 * Export as OSM XML
	 * @param cells An array of cells
	 */
	exportAsOSMXML: function(cells) {
		//Export
		let blob = new Blob(
			[osm_geojson.geojson2osm(makeGeoJSON(cells))],
			{type: "text/xml;charset=utf-8"}
		);
		FileSaver.saveAs(blob, "grid.osm");
	}
};

module.exports = GridExporter;
